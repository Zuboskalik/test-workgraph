<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class EmployeeScheduleController extends Controller
{
    public function getWorkSchedule(request $request): JsonResponse
    {
        $employeePlaceholder = [
            1 => [
                'name' => 'First Worker',
                'timeRanges' =>  [
                    [
                        'start' => '10:00',
                       'end' => '13:00'
                    ],
                    [
                        'start' => '14:00',
                       'end' => '19:00'
                    ]
                 ]
            ],
            2 => [
                'name' => 'Second Employee',
                'timeRanges' =>  [
                    [
                        'start' => '9:00',
                       'end' => '12:00'
                    ],
                    [
                        'start' => '13:00',
                       'end' => '18:00'
                    ]
                 ]
            ],

        ];

        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');
        $employeeId = $request->input('employeeId');
        if (!$startDate || !$endDate || !$employeeId) {
            return new JsonResponse(['Not provided requested data or id'], JsonResponse::HTTP_BAD_REQUEST);
        }
        
        $endDateYear = Carbon::createFromFormat('Y-m-d', $endDate)->year;
        $startDateYear = Carbon::createFromFormat('Y-m-d', $startDate)->year;

        $client = new Client();
        $calendarHolidays = [];
        for ($y = $startDateYear; $y <= $endDateYear; $y++) {
            $calendarAPI = 'http://xmlcalendar.ru/data/ru/' . $y .'/calendar.json';
            $res = $client->get($calendarAPI);
            $calendarHolidays[$y]  = json_decode($res->getBody()->getContents(), true);
        }

        $period = CarbonPeriod::create($startDate, $endDate);

        $scheduleArray = [];
        $employee = $employeePlaceholder[$employeeId];
        foreach ($period as $date) {
            $monthList = $calendarHolidays[$date->format('Y')]['months'];

            $dateCheck = $this->searchForMonth((int)$date->format('m'), $monthList);

            if ($dateCheck !== null) {
                //intval используется для записей вида '20*';
                $offDaysList = array_map('intval', explode(',', $monthList[$dateCheck]['days']));
            }

            if (!in_array($date->format('d'), $offDaysList)) {
                $scheduleArray[] = [
                    'day' => $date->format('Y-m-d'),
                    'timeRanges' => $employee['timeRanges'],
                ];
            }
        }

        $result = ['schedule' => $scheduleArray];
        return new JsonResponse($result, JsonResponse::HTTP_OK);
    }

    function searchForMonth($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['month'] === $id) {
                return $key;
            }
        }
        return null;
    }
}
